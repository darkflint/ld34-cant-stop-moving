var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fs = require("fs");
var routes = require('./routes/index');

var multer  = require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/music/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        if(file.originalname.match(/\.mp3$/) &&
            (file.mimetype === "audio/mpeg"
            || file.mimetype === "audio/mp3")) {
            cb(null, true);
        } else {
            cb(null, false);
        }
    }
});

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.set('port', "3434");

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/', upload.single('music_file'), function (req, res, next) {
    fs.readdir(__dirname + "/public/music/" , function (err, files) {
        if (err) {
            console.error(err);
            res.render('index', {
                title: 'LD34 Cant Stop Moving',
                alert: "Couldnt load songs"
            });
            return;
        }

        var files_array = [];
        for(var i = 0; i < files.length; i++) {
            var name = files[i].replace(new RegExp("_", 'g'), " ");
            name = name.replace(".mp3", "");

            files_array.push({
                file: files[i],
                name: name
            });
        }

        res.render('index', {
            title: 'LD34 Cant Stop Moving',
            message: "Song saved!",
            files: files_array
        });
    });
});

app.use("/", routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;

http.listen("xxxx");

var user_connected = 0;
io.on("connection", function(socket) {
    user_connected += 1;
    io.emit("connected", user_connected);

    socket.on("chat_send", function(data) {
        io.emit("chat_msg", data);
    });

    socket.on('disconnect', function() {
        user_connected -= 1;
        io.emit("connected", user_connected);
    });
});