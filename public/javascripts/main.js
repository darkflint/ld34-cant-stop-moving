ld34.main = (function()  {
    var socket = io();
    var supportAudio = true;

    var audioCtx;
    var source;

    var error_box = document.querySelector(".error");

    var highScore = document.querySelector("#highScore");
    var scoreRound = highScore.querySelector("div");
    var scoreTable = highScore.querySelector("table");

    var score = 0;

    try {
        // Fix up for prefixing
        audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    }
    catch(e) {
        supportAudio = false;
        document.body.className = "no_audio_api";
    }

    var elem_audio = document.getElementsByTagName("audio")[0];
    var elem_select = document.querySelector("select");
    var chatbox_counter = document.querySelector("#chatbox header p");
    var chat = document.querySelector("#chatbox ul");
    var chat_send = document.querySelector("#chat_send");
    var chat_name = document.querySelector("#chat_name");
    var chat_msg = document.querySelector("#chat_msg");

    elem_audio.src = "music/" +  elem_select.value;

    var localStore = JSON.parse(localStorage.getItem("Score"));
    if(localStore) {
        var tbody = scoreTable.querySelector("tbody")

        for(var i = 0; i < localStore.length; i++) {
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var td2 = td.cloneNode();

            td.innerHTML = localStore[i].score;
            td2.innerHTML = localStore[i].name;

            tr.appendChild(td);
            tr.appendChild(td2);
            tbody.appendChild(tr);
        }
    }

    if(supportAudio) {
        source = audioCtx.createMediaElementSource(elem_audio);
    }

    elem_select.addEventListener("change", function() {
        score = 0;
        scoreRound.innerHTML = score;
        elem_audio.src = "music/" +  this.value;

        if(supportAudio) {
            source = audioCtx.createMediaElementSource(elem_audio);
        }
    });

    chat_msg.addEventListener("keypress", function(e) {
        var char = e.keyCode;

        if(char === 13) {
            send_msg();
        }
    });

    chat_send.addEventListener("click", function() {
        send_msg();
    });

    socket.on("chat_msg", function(data) {
        var li = document.createElement("li");
        var span_name = document.createElement("span");
        var span_text = document.createElement("span");

        span_name.innerHTML = data.name + ":";
        span_text.innerHTML = data.msg;

        li.appendChild(span_name);
        li.appendChild(span_text);
        chat.appendChild(li);
    });

    socket.on("connected", function(data) {
        chatbox_counter.innerHTML = data + " users online";
    });

    if (audioCtx) {
        var drawVisual;

        var progress = document.querySelector("progress");

        var buttonA = document.querySelector("#buttonA span");
        var buttonB = document.querySelector("#buttonB span");
        var elem_input_file = document.querySelector("#select_file");

        var canvas = document.querySelector('canvas');
        var canvasCtx = canvas.getContext('2d');

        var WIDTH = 800;
        var HEIGHT = 500;

        elem_input_file.value = "";

        canvas.width = WIDTH;
        canvas.height = HEIGHT;

        var analyser = audioCtx.createAnalyser();

        var bufferLength = analyser.frequencyBinCount;
        var dataArray = new Uint8Array(bufferLength);

        var music_name;

        source.connect(analyser);
        analyser.connect(audioCtx.destination);

        var press_a = false;
        var press_d = false;
        document.addEventListener("keydown", function(e) {
            //keyCode 65: a
            //keyCode 68: d
            var char = e.keyCode;
            var isPlaying = !elem_audio.paused;
            if(isPlaying && (char === 65 || char === 68)) {
                if(press_a && char === 65) {
                    score += 100;
                    scoreRound.innerHTML = score;
                    press_a = false;
                } else if(press_d && char === 68) {
                    score += 100;
                    scoreRound.innerHTML = score;
                    press_d = false;
                }
            }
        });

        var timeOut = false;
        elem_audio.addEventListener("playing", function (e) {
            music_name = elem_audio.src;
            canvasCtx.clearRect(0, 0, WIDTH, HEIGHT);
            draw();

            var timer = Math.round(Math.random() * 10) * 300;

            timeOut = function () {
                setTimeout(function () {
                    var isPlaying = !elem_audio.paused;
                    if(isPlaying) {
                        analyser.getByteTimeDomainData(dataArray);
                        var press_time = dataArray[0] * 3;
                        var randomNum = Math.round(Math.random());
                        if (randomNum) {
                            buttonA.innerHTML = "A";
                            buttonA.style.border = "3px solid red";
                            press_a = true;

                            setTimeout(function() {
                                press_a = false;
                                buttonA.innerHTML = "";
                                buttonA.removeAttribute("style");
                            }, press_time);
                        } else {
                            buttonB.innerHTML = "D";
                            buttonB.style.border = "3px solid red";
                            press_d = true;
                            setTimeout(function() {
                                buttonB.innerHTML = "";
                                buttonB.removeAttribute("style");
                                press_d = false;
                            }, press_time);
                        }

                        timer = dataArray[0] * 15;
                        timeOut();
                    }
                }, timer);
            };

            timeOut();
        });

        elem_audio.addEventListener("pause", function () {
            window.cancelAnimationFrame(drawVisual);
        });

        elem_audio.addEventListener("ended", function () {
            window.cancelAnimationFrame(drawVisual);
            var tr = document.createElement("tr");
            var td = document.createElement("td");
            var td2 = td.cloneNode();

            td.innerHTML = score;
            td2.innerHTML = music_name;

            tr.appendChild(td);
            tr.appendChild(td2);

            scoreTable.getElementsByTagName("tbody")[0].appendChild(tr);

            var scores = [];

            if(localStore) {
                scores = localStore;
            }

            scores.push({
                score: score,
                name: music_name
            });

            localStorage.setItem("Score", JSON.stringify(scores));
        });

        function draw() {
            drawVisual = requestAnimationFrame(draw);

            analyser.getByteTimeDomainData(dataArray);

            canvasCtx.fillStyle = 'rgb(0, 0, 0)';
            canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

            var barWidth = (WIDTH / bufferLength) * 5;
            var barHeight;
            var x = 0;

            for (var i = 0; i < bufferLength; i++) {
                barHeight = dataArray[i] / 2;
                canvasCtx.fillStyle = 'rgb(' + (barHeight + 100) + ',50,50)';
                canvasCtx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);

                x += barWidth + 1;
            }
        }

        function send_msg() {
            var name = chat_name.value;
            var msg = chat_msg.value;
            var error = false;

            if(name.length < 3) {
                error_box.innerHTML = "Name too short ";
                error = true;
            }

            if(msg.length < 2) {
                error_box.innerHTML += "Message too short";
                error = true;
            }

            if(!error) {
                error_box.innerHTML = "";
                socket.emit("chat_send", {
                    name: chat_name.value,
                    msg: chat_msg.value
                });
                chat_msg.value = "";
            }
        }
    }
})();