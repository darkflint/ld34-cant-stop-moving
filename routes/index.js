var fs = require("fs");

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    fs.readdir(__dirname + "/../public/music/" , function (err, files) {
        if (err) {
            console.error(err);
            res.render('index', {
                title: 'LD34 Cant Stop Moving',
                alert: "Couldnt load songs"
            });
            return;
        }

        var files_array = [];
        for(var i = 0; i < files.length; i++) {
            var name = files[i].replace(new RegExp("_", 'g'), " ");
            name = name.replace(".mp3", "");

            files_array.push({
                file: files[i],
                name: name
            });
        }

        res.render('index', {
            title: 'LD34 Cant Stop Moving',
            files: files_array
        });
    });

});

module.exports = router;
